import 'package:flutter/material.dart';

import 'age_app.dart';
import 'constants.dart';
import 'strings.dart';

void main() => runApp(
      MaterialApp(
        title: Strings.appTitle,
        theme: ThemeData(primaryColor: Constants.primaryColor),
        home: AgeApp(),
      ),
    );