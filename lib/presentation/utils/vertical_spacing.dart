import 'package:flutter/material.dart';

class VerticalSpacing extends SizedBox {
  VerticalSpacing({double height = 14.0}) : super(height: height);
}