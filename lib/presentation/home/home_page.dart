import 'package:flutter/material.dart';

import '../../constants.dart';
import '../../data/model/person.dart';
import '../../strings.dart';
import '../utils/vertical_spacing.dart';
import 'home_view_model.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with WidgetsBindingObserver {
  TextEditingController userController = new TextEditingController();

  final HomeViewModel _homeViewModel = HomeViewModel();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    _homeViewModel.closeObservable();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(50.0),
      child: Scaffold(
        body: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              StreamBuilder<Person>(
                stream: _homeViewModel.personData,
                builder: (context, snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.none:
                      return Text(Strings.guessAgeDescription);
                    case ConnectionState.active:
                      if (snapshot.hasData) {
                        var person = snapshot.data;
                        return Text(
                            "Your guessed age is ${person.age.toString()} years");
                      } else if (snapshot.hasError) {
                        return Text("${snapshot.error}");
                      }
                      return Text(Strings.guessAgeDescription);
                    case ConnectionState.waiting:
                      if (userController.text.isNotEmpty) {
                        return Theme(
                          data: Theme.of(context)
                              .copyWith(accentColor: Constants.primaryColor),
                          child: new CircularProgressIndicator(),
                        );
                      }
                      return Text(Strings.guessAgeDescription);
                    case ConnectionState.done:
                      return Text(Strings.guessAgeDescription);
                  }
                  return Text(Strings.guessAgeDescription);
                },
              ),
              VerticalSpacing(height: 30),
              TextField(
                controller: userController,
                decoration: InputDecoration(
                  labelText: Strings.nameLabelText,
                  hintText: Strings.nameLabelHint,
                  icon: Icon(Icons.person),
                ),
              ),
              VerticalSpacing(),
              RaisedButton(
                child: Text(Strings.ageButtonText),
                onPressed: () {
                  _fetchUserData();
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  void _fetchUserData() {
    _homeViewModel.getPersonData(userController.text);
    setState(() {});
  }
}
