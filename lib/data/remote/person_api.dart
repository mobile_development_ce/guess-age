import 'package:Age/data/model/person.dart';

abstract class PersonApi {
  Future<Person> getPersonData(String name);
}