import 'package:json_annotation/json_annotation.dart';

part 'person.g.dart';

@JsonSerializable()
class Person {

  @JsonKey(name: 'name')
  final String name;
  @JsonKey(name: 'age')
  final int age;

  Person({
      this.age,
      this.name
  });

  factory Person.fromJson(Map<String, dynamic> json) => _$PersonFromJson(json);

  Map<String, dynamic> toJson() => _$PersonToJson(this);
}