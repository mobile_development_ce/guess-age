import 'package:flutter/material.dart';

import 'strings.dart';
import 'presentation/home/home_page.dart';

class AgeApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(Strings.appTitle),
      ),
      body: HomePage(),
    );
  }
}