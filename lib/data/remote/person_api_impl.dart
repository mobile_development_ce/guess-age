import 'dart:convert';

import 'package:http/http.dart' as http;

import '../model/person.dart';
import 'person_api.dart';

class PersonApiImpl implements PersonApi {
  String dataURL = "https://api.agify.io/?name=";

  @override
  Future<Person> getPersonData(String personName) async {
    var person;
    var url = dataURL;

    url += "$personName";

    http.Response response = await http.get(url);
    final apiResponse = json.decode(response.body);

    person = Person.fromJson(apiResponse);

    return person;
  }
}
