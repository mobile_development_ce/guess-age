import 'package:Age/data/remote/person_api_impl.dart';
import 'package:Age/data/repository/person_repository_impl.dart';
import 'package:Age/domain/get_person_data_use_case.dart';
import 'package:rxdart/rxdart.dart';

import '../../data/model/person.dart';
import '../../strings.dart';

class HomeViewModel {
  var personDataSubject = PublishSubject<Person>();

  Observable<Person> get personData => personDataSubject.stream;
  GetPersonDataUseCase getPersonDataUseCase =
      GetPersonDataUseCase(PersonRepositoryImpl(PersonApiImpl()));

  void getPersonData(String personName) async {
    try {
      if (personName.isNotEmpty) {
        personDataSubject = PublishSubject<Person>();
        var personData = await getPersonDataUseCase.perform(personName);
        if(null != personData.age){
          personDataSubject.sink.add(personData);
        } else {
          personDataSubject.sink.addError(Strings.invalidNameText);
        }
      } else {
        personDataSubject.sink.addError(Strings.guessAgeDescription);
      }
    } catch (e) {
      await Future.delayed(Duration(milliseconds: 500));
      personDataSubject.sink.addError(e);
    }
  }

  void closeObservable() {
    personDataSubject.close();
  }
}
