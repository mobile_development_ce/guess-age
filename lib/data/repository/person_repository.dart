import '../model/person.dart';

abstract class PersonRepository {
  Future<Person> getPersonData(String name);
}
