import '../model/person.dart';
import '../remote/person_api.dart';
import 'person_repository.dart';

class PersonRepositoryImpl implements PersonRepository {
  PersonApi personApi;

  PersonRepositoryImpl(PersonApi personApi) {
    this.personApi = personApi;
  }

  @override
  Future<Person> getPersonData(String name) {
    return personApi.getPersonData(name);
  }
}
