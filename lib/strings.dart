class Strings {
  static String appTitle = "Guess an age";
  static String guessAgeDescription = "Please, enter a name to guess an age";
  static String nameLabelText = "Name";
  static String nameLabelHint = "Carlos";
  static String ageButtonText = "Get age";
  static String invalidNameText = "Please, enter a valid name";

}