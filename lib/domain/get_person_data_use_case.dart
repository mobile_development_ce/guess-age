import '../data/model/person.dart';
import '../data/repository/person_repository_impl.dart';

class GetPersonDataUseCase {
  PersonRepositoryImpl personRepository;

  GetPersonDataUseCase(PersonRepositoryImpl personRepository) {
    this.personRepository = personRepository;
  }

  Future<Person> perform (String personName) {
    return personRepository.getPersonData(personName);
  }
}